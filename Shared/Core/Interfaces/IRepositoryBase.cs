﻿namespace Core.Interfaces
{
    public interface IRepositoryBase 
    {
        void Save();
    }
}