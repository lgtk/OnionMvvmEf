﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface ILogger
    {
        void Error(Exception ex, string msg);
        void FatalError(Exception ex, string msg);
        void Info(string msg);
        void Debug(string msg);
        void Trace(string msg);
        
    }
}
