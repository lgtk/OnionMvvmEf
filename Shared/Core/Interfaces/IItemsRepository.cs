﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IItemsRepository : IRepositoryBase
    {
        IEnumerable<Item> GetItems(int page = 0, int pageSize = 100);
        IEnumerable<Item> SearchItems(string text,int page = 0, int pageSize = 100);
        void Edit(Item item);
        void Remove(Item item);
        void AddNew(Item item);
    }
}
