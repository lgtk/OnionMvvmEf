﻿using Core.Entities;
using Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IDocumentRepository : IRepositoryBase
    {
        IEnumerable<Document> GetPage(int page = 0, int pageSize = 100, SortingEnum sorting = SortingEnum.Desc);

        Document GetDocument(int id);

        IEnumerable<Document> SearchDocuments(Expression<Func<Document, bool>> search, int page = 0, int pageSize = 0, SortingEnum sorting = SortingEnum.Desc);

        void AddItemsToDocument(IEnumerable<DocumentItem> items);

        void RemoveDocumentItem(int documentItemId);

        void Add(Document document);
    }
}
