﻿using Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Document
    {
        [Key]
        public int Id { get; set; }

        public string Description { get; set; }

        public string DocumentNumber { get; set; }

        public int ClientId { get; set; }

        public virtual Client Client { get; set; }

        public decimal TotalPrieceNet { get; set; }
   
        public IList<DocumentItem> Items { get; set; }

        public DateTime SaveDate { get; set; } = DateTime.UtcNow;

        public DateTime? ModyfiDate { get; set; }

        public int Status { get; set; }

        public int CurrencyId { get; set; }

        public virtual Currency Currency { get; set; }


        public int DocumentTypeId { get; set; }

        public DocumentType DocumentType { get; set; }
        public decimal TotalPrieceWithTax { get; set; }
    }
}
