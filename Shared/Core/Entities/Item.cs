﻿using Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core.Entities
{
    public class Item
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal PrieceNet { get; set; }

        public decimal PrieceWithTax { get; set; }

        public int TaxId { get; set; }

        public virtual  Tax Tax { get; set; }

        public DateTime SaveDate { get; set; } = DateTime.UtcNow;

        public DateTime? ModyfiDate { get; set; }

        public int Status { get; set; }

    }
}