﻿using Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core.Entities
{
    public class Client
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public virtual IList<Document> Documents { get; set; }

        public DateTime SaveDate { get; set; } = DateTime.UtcNow;

        public DateTime? ModyfiDate { get; set; }

        public int Status { get; set; }
    }
}