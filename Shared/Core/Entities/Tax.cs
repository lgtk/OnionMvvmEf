﻿using Core.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Entities
{
    public class Tax
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public int Value { get; set; }

        public DateTime SaveDate { get; set; } = DateTime.UtcNow;

        public DateTime? ModyfiDate { get; set; }

        public int Status { get; set; }
    }
}