﻿using Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class DocumentType
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public DateTime SaveDate { get; set; } = DateTime.UtcNow;

        public DateTime? ModyfiDate { get; set; }

        public int Status { get; set; }
    }
}
