﻿using Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class DocumentItem
    {
        [Key]
        public int Id { get; set; }

        public int DocumentId { get; set; }

        public int ItemId { get; set; }

        public int Count { get; set; }

        public virtual Document Document { get; set; }

        public virtual Item Item { get; set; }

        public decimal PrieceNet { get; set; }

        public decimal PrieceWithTax { get; set; }

        public DateTime SaveDate { get; set; } = DateTime.UtcNow;

        public DateTime? ModyfiDate { get; set; }

        public int Status { get; set; }
    }
}
