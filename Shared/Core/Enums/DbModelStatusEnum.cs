﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enums
{
    public enum DbModelStatusEnum : ushort
    {
        Active = 0,
        
        Deleted = 2
    }
}
