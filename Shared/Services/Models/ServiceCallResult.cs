﻿using Services.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Models
{

    public class ServiceCallResult
    {
        public static ServiceCallResult CreateError(string message)
        {
            return new ServiceCallResult { State = ServiceCallState.Error, Message = message };
        }

        public static ServiceCallResult Create()
        {
            return new ServiceCallResult { State = ServiceCallState.Success, Message = string.Empty };
        }

        private ServiceCallResult()
        {

        }
        public string Message { get; private set; }
        public ServiceCallState State { get; private set; }
      
    }
    public class ServiceCallResult<T> where T : class
    {
        public static ServiceCallResult<T> CreateError(string message)
        {
            return new ServiceCallResult<T> { State = ServiceCallState.Error, Message = message };
        }

        public static ServiceCallResult<T> Create(T result)
        {
            return new ServiceCallResult<T> { State = ServiceCallState.Success, Message = string.Empty, Result = result };
        }

        private ServiceCallResult()
        {

        }
        public string Message { get; private set; }
        public ServiceCallState State { get; private set; }
        public T Result { get; private set; }
    }
}
