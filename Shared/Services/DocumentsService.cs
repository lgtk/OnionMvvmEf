﻿using Core.Entities;
using Core.Enums;
using Core.Interfaces;
using Infrastructure.Cache;
using Services.Interfaces;
using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class DocumentsService : ServiceBase, IDocumentsService
    {
        private readonly IDocumentRepository documentRepository;
        private readonly IDocumentTypeRepository documentTypeRepository;

        public DocumentsService(IDocumentRepository documentRepository, IDocumentTypeRepository documentTypeRepository, ILoggerFactory loggerFactory) : base("DocumentsService", loggerFactory)
        {
            this.documentRepository = documentRepository;
            this.documentTypeRepository = documentTypeRepository;
        }

        public async Task<ServiceCallResult> AddDocumentAsync(Document document, IEnumerable<DocumentItem> items)
        {
            return await Task.Run(() => AddDocument(document, items));

        }

        public ServiceCallResult AddDocument(Document document, IEnumerable<DocumentItem> items)
        {

            var ret = Try(() =>
            {
                documentRepository.Add(document);
                AssignDocumentToItems(document, items);
                documentRepository.AddItemsToDocument(items);
            }, "Błąd dodawania dokumentu");
            return ret;

        }

        private void AssignDocumentToItems(Document document, IEnumerable<DocumentItem> items)
        {
            foreach (var item in items)
            {
                item.Item = null;
                item.Document = document;
                item.DocumentId = document.Id;
            }
        }

        public ServiceCallResult UpdateDocument(Document document, IEnumerable<DocumentItem> items)
        {

            var ret = Try(() =>
            {
                var doc = documentRepository.GetDocument(document.Id);
                doc.CurrencyId = document.CurrencyId;
                doc.TotalPrieceNet = document.TotalPrieceNet;
                doc.TotalPrieceWithTax = document.TotalPrieceWithTax;
                documentRepository.Save();
                AssignDocumentToItems(doc, items);
                documentRepository.AddItemsToDocument(items.Where(x => x.Id == 0));

            }, "Błąd aktualizacji dokumentu");
            return ret;
        }

        public async Task<ServiceCallResult<IEnumerable<DocumentType>>> GetDocumentTypesAsync()
        {
            return await Task.Run(() => GetDocumentTypes());
        }


        public ServiceCallResult<IEnumerable<DocumentType>> GetDocumentTypes()
        {

            var cache = new GenericCache<DocumentType>();
            var ret = Try(() => cache.GetOrAdd(() => documentTypeRepository.GetAll()), "Błąd pobierania typów dokumentów");
            return ret;
        }

        public ServiceCallResult<IEnumerable<Document>> GetDocuments()
        {
            var ret = Try(() => documentRepository.GetPage(), "Błąd pobierania dokumentów");

            return ret;
        }

        public ServiceCallResult<IEnumerable<Document>> SearchDocuments(string phrase)
        {
            var ret = Try(() => documentRepository.SearchDocuments(x => x.Status != (int)DbModelStatusEnum.Deleted && x.DocumentNumber.Contains(phrase)), "Błąd wyszukiwania dokumentów");
            return ret;
        }

        public async Task<ServiceCallResult<IEnumerable<Document>>> SearchDocumentsAsync(string phrase)
        {
            return await Task.Run(() => SearchDocuments(phrase));
        }

        public async Task<ServiceCallResult<IEnumerable<Document>>> GetDocumentsAsync()
        {
            return await Task.Run(() => GetDocuments());
        }


        public ServiceCallResult<string> GenerateDocumentNumber(DocumentType documentType, Client client)
        {

            var ret = Try(() => $"{documentType.Code}/{client.Code}/{DateTime.UtcNow.ToString("yyyyMMddHHmmssfff")}", "Błąd generowania numeru dokumentu");

            return ret;
        }
        public async Task<ServiceCallResult> RemoveAsync(DocumentItem selectedDocumentItem)
        {
            return await Task.Run(() => Remove(selectedDocumentItem));
        }
        public ServiceCallResult Remove(DocumentItem selectedDocumentItem)
        {
            var res = Try(() => TryRemove(selectedDocumentItem), "Błąd kasowania pozycji dokumentu");
            return res;

        }

        private void TryRemove(DocumentItem selectedDocumentItem)
        {
            if (selectedDocumentItem.Id == 0)
            {
                return;
            }
            documentRepository.RemoveDocumentItem(selectedDocumentItem.Id);
        }

        public ServiceCallResult Remove(Document selectedDocument)
        {
            ServiceCallResult res;
            res = Try(() => TryRemove(selectedDocument),"Błąd kasowania dokumentu");

            return res;

        }

        private void TryRemove(Document selectedDocument)
        {

            var doc = documentRepository.GetDocument(selectedDocument.Id);
            doc.Status = (int)DbModelStatusEnum.Deleted;
            documentRepository.Save();
        }

        public async Task<ServiceCallResult> RemoveAsync(Document selectedDocument)
        {
            return await Task.Run(() => Remove(selectedDocument));
        }
    }
}
