﻿using Core.Entities;
using Core.Interfaces;
using Services.Interfaces;
using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ItemService : ServiceBase, IItemService
    {
        private readonly IItemsRepository itemRepository;

        public ItemService(IItemsRepository itemRepository, ILoggerFactory loggerFactory) : base("ItemService", loggerFactory)
        {
            this.itemRepository = itemRepository;
        }
        public ServiceCallResult<IEnumerable<Item>> GetItems(int page = 0, int pageSize = 100)
        {
            return Try(() => itemRepository.GetItems(page, pageSize), "Wystąpił błąd pobierania przedmiotów");

        }
        public ServiceCallResult<IEnumerable<Item>> SearchItems(string text, int page = 0, int pageSize = 100)
        {
            return Try(() => itemRepository.SearchItems(text, page, pageSize), "Wystąpił błąd wyszukiwania przedmiotów");
        }

        public ServiceCallResult Edit(Item item)
        {
            return Try(() => itemRepository.Edit(item), "Błąd edycji");
        }
        public ServiceCallResult Remove(Item item)
        {
            return Try(() => itemRepository.Remove(item), "Błąd kasowania");
        }
        public ServiceCallResult AddNew(Item item)
        {
            return Try(() => itemRepository.AddNew(item), "Błąd dodawania przedmiotu");
        }

        public async Task<ServiceCallResult<IEnumerable<Item>>> GetItemsAsync(int page = 0, int pageSize = 100)
        {
            return await Task.Run(() => GetItems(page, pageSize));
        }
        public async Task<ServiceCallResult<IEnumerable<Item>>> SearchItemsAsync(string text, int page = 0, int pageSize = 100)
        {
            return await Task.Run(() => SearchItems(text, page,pageSize));
        }
    }
}
