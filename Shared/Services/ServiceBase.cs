﻿using Core.Interfaces;
using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ServiceBase
    {
        protected readonly ILogger logger;

        public ServiceBase(string name, ILoggerFactory loggerFactory)
        {
            logger = loggerFactory.CreateLogger(name);
        }

        protected ServiceCallResult<T> Try<T>(Func<T> func, string errorMessage) where T : class
        {
            ServiceCallResult<T> ret;
            try
            {
                var funcResult = func();
                if(funcResult == null)
                {
                    throw new NullReferenceException("NPE!");
                }
                ret = ServiceCallResult<T>.Create(funcResult);
            }
            catch (Exception ex)
            {
                ret = ServiceCallResult<T>.CreateError(errorMessage);
                logger.Error(ex, "error in " + func.Method);
            }
            return ret;
        }

        protected ServiceCallResult Try(Action func, string errorMessage)
        {
            ServiceCallResult ret;
            try
            {
                func();
                ret = ServiceCallResult.Create();
            }
            catch (Exception ex)
            {
                ret = ServiceCallResult.CreateError(errorMessage);
                logger.Error(ex, "error in " + func.Method);
            }
            return ret;
        }
    }
}
