﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Services.Models;

namespace Services.Interfaces
{
    public interface ITaxService
    {
        ServiceCallResult<IEnumerable<Tax>> GetTaxes();
        Task<ServiceCallResult<IEnumerable<Tax>>> GetTaxesAsync();
    }
}