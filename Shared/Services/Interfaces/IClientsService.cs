﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Services.Models;

namespace Services.Interfaces
{
    public interface IClientsService
    {
        ServiceCallResult<IEnumerable<Client>> GetClients();
        Task<ServiceCallResult<IEnumerable<Client>>> GetClientsAsync();
    }
}