﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Services.Models;

namespace Services.Interfaces
{
    public interface IItemService
    {
        ServiceCallResult<IEnumerable<Item>> GetItems(int page = 0, int pageSize = 100);
        ServiceCallResult<IEnumerable<Item>> SearchItems(string text, int page = 0, int pageSize = 100);
        Task<ServiceCallResult<IEnumerable<Item>>> SearchItemsAsync(string text, int page = 0, int pageSize = 100);
        Task<ServiceCallResult<IEnumerable<Item>>> GetItemsAsync(int page = 0, int pageSize = 100);
        ServiceCallResult Edit(Item item);
        ServiceCallResult Remove(Item item);
        ServiceCallResult AddNew(Item item);
    }
}