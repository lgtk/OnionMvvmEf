﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Services.Models;

namespace Services.Interfaces
{
    public interface ICurrencyService
    {
        ServiceCallResult<IEnumerable<Currency>> GetCurrencies();
        Task<ServiceCallResult<IEnumerable<Currency>>> GetCurrenciesAsync();
    }
}