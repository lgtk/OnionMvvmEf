﻿using Core.Entities;
using Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IDocumentsService
    {
        ServiceCallResult<string> GenerateDocumentNumber(DocumentType documentType, Client client);
        ServiceCallResult AddDocument(Document document, IEnumerable<DocumentItem> items);
        ServiceCallResult<IEnumerable<DocumentType>> GetDocumentTypes();
        ServiceCallResult Remove(DocumentItem selectedDocumentItem);
        ServiceCallResult Remove(Document selectedDocument);
        ServiceCallResult<IEnumerable<Document>> GetDocuments();

        Task <ServiceCallResult> AddDocumentAsync(Document document, IEnumerable<DocumentItem> items);
        Task<ServiceCallResult<IEnumerable<DocumentType>>> GetDocumentTypesAsync();
        Task<ServiceCallResult> RemoveAsync(DocumentItem selectedDocumentItem);
        Task<ServiceCallResult> RemoveAsync(Document selectedDocument);
        Task<ServiceCallResult<IEnumerable<Document>>> GetDocumentsAsync();

        ServiceCallResult UpdateDocument(Document document, IEnumerable<DocumentItem> items);

        ServiceCallResult<IEnumerable<Document>> SearchDocuments(string phrase);
        Task<ServiceCallResult<IEnumerable<Document>>> SearchDocumentsAsync(string phrase);
    }
}