﻿using System.Threading.Tasks;
using Core.Entities;
using Services.Models;

namespace Services.Interfaces
{
    public interface IGitHubService
    {
        ServiceCallResult<GitHubApiResponse> GetRepositoryData(string login, string repositoryName);
        Task<ServiceCallResult<GitHubApiResponse>> GetRepositoryDataAsync(string login, string repositoryName);
    }
}