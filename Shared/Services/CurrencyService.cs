﻿using Core.Entities;
using Core.Interfaces;
using Infrastructure.Cache;
using Services.Interfaces;
using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class CurrencyService : ServiceBase, ICurrencyService
    {
        private readonly ICurrencyRepository currencyRepository;

        public CurrencyService(ICurrencyRepository currencyRepository, ILoggerFactory loggerFactory) : base("CurrencyService", loggerFactory)
        {
            this.currencyRepository = currencyRepository;
        }

        public ServiceCallResult<IEnumerable<Currency>> GetCurrencies()
        {

            var cacheData = new GenericCache<Currency>();
            var ret = Try(() => cacheData.GetOrAdd(() => currencyRepository.GetCurrencies()), "Wystąpił błąd pobierania walut");
            return ret;
        }

        public async Task<ServiceCallResult<IEnumerable<Currency>>> GetCurrenciesAsync()
        {
            return await Task.Run(() => GetCurrencies());
        }
    }
}
