﻿using Core.Entities;
using Core.Interfaces;
using Infrastructure.Cache;
using Services.Interfaces;
using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ClientsService : ServiceBase, IClientsService
    {
        private readonly IClientRepository clientRepository;

        public ClientsService(IClientRepository clientRepository, ILoggerFactory loggerFactory) : base("ClientsService", loggerFactory)
        {
            this.clientRepository = clientRepository;
        }

        public ServiceCallResult<IEnumerable<Client>> GetClients()
        {
            var cache = new GenericCache<Client>();
            var ret = Try(() => cache.GetOrAdd(() => clientRepository.GetAll()), "Błąd Pobierania listy klientów");
            return ret;
        }

        public async Task<ServiceCallResult<IEnumerable<Client>>> GetClientsAsync()
        {
            return await Task.Run(() => GetClients());
        }
    }
}
