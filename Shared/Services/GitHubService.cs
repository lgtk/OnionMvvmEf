﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Services.Interfaces;
using Services.Models;

namespace Services
{
    public class GitHubService : ServiceBase, IGitHubService
    {
        private readonly IRepositoryRestApi api;

        public GitHubService(IRepositoryRestApi api, ILoggerFactory loggerFactory) : base("GitHubService", loggerFactory)
        {
            this.api = api;
        }

        public ServiceCallResult<GitHubApiResponse> GetRepositoryData(string login, string repositoryName)
        {
            return Try(() => api.GetRepositoryData(login, repositoryName), "Wystąpił błąd pobierania danych repozytorium");
        }

        public async Task<ServiceCallResult<GitHubApiResponse>> GetRepositoryDataAsync(string login, string repositoryName)
        {
            return await Task.Run(() => GetRepositoryData(login, repositoryName));
        }
    }
}
