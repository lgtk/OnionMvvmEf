﻿using Core.Entities;
using Core.Interfaces;
using Services.Interfaces;
using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class TaxService : ServiceBase, ITaxService
    {
        private readonly ITaxRepository taxRepository;

        public TaxService(ITaxRepository taxRepository, ILoggerFactory loggerFactory) : base("TaxService", loggerFactory)
        {
            this.taxRepository = taxRepository;
        }
        public ServiceCallResult<IEnumerable<Tax>> GetTaxes()
        {
            return Try(() => taxRepository.GetTaxes(), "Błąd pobierania podatków");
        }

        public async Task<ServiceCallResult<IEnumerable<Tax>>> GetTaxesAsync()
        {
            return await Task.Run(() => GetTaxes());
        }
    }
}
