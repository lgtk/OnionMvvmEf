﻿using Core.Entities;
using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class MsSqlContext : DbContext, IContext
    {
        public MsSqlContext(IAppConfiguration configuration) : base(configuration.ConnectionString)
        {
#if DEBUG
            CreateDemoData();
#endif

        }

        public MsSqlContext() : base("Default")
        {

        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Tax> Taxes { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<DocumentItem> DocumentItems { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }

        #region Create Demo data
        /// <summary>
        /// Only for demo
        /// </summary>
        private void CreateDemoData()
        {
            var anyAdded = false;
            anyAdded = AddDocumentsIfNeed() || AddTaxesIfNeed() || AddClientIfNeed() || AddItemsIfNeed() || AddCurrencyIfNeed();

            if (anyAdded)
            {
                SaveChanges();
            }

        }

        private bool AddClientIfNeed()
        {
            if (Clients.Any())
            {
                return false;
            }
            for (int i = 0; i < 10; i++)
            {
                Clients.Add(new Client
                {
                    Code = $"CC-{i}",
                    Name = $"Client Nr. {i}",
                    Description = $"Desc...",

                });
            }


            return true;

        }
        private bool AddItemsIfNeed()
        {
            if (Items.Any())
            {
                return false;
            }
            var rnd = new Random();
            var lastTax = Taxes.FirstOrDefault();
            for (int i = 0; i < 10; i++)
            {
                Items.Add(new Item
                {
                    Name = $"Item nr {i}",
                    PrieceNet = rnd.Next(10, 1000),
                    TaxId = lastTax.Id

                });
            }


            return true;

        }

        private bool AddTaxesIfNeed()
        {
            if (Taxes.Any())
            {
                return false;
            }
            var taxesToAdd = new List<KeyValuePair<string, int>>
                {
                    { new KeyValuePair<string, int>("8%",8) },
                    { new KeyValuePair<string, int>("23%",23) },

                };
            taxesToAdd.ForEach(kv => Taxes.Add(new Tax
            {
                Name = kv.Key,
                Value = kv.Value,


            }));


            return true;

        }

        private bool AddCurrencyIfNeed()
        {
            if (Currencies.Any())
            {
                return false;
            }
            var currenciesToAdd = new List<KeyValuePair<string, string>>
                {
                    { new KeyValuePair<string, string>("PLN","PLN") },
                    { new KeyValuePair<string, string>("EURO","EURO") },
                     { new KeyValuePair<string, string>("USD","USD") },

                };
            currenciesToAdd.ForEach(kv => Currencies.Add(new Currency
            {
                Name = kv.Key,
                Code = kv.Value,


            }));


            return true;

        }
        private bool AddDocumentsIfNeed()
        {
            if (DocumentTypes.Any())
            {
                return false;
            }
            var docsToAdd = new List<KeyValuePair<string, string>>
                {
                    { new KeyValuePair<string, string>("PZ","Przyjęcie zewnętrzne") },
                    { new KeyValuePair<string, string>("PW","Przyjęcie wewnętrzne") },
                    { new KeyValuePair<string, string>("ZW","Zwrot zewnętrzny") },
                    { new KeyValuePair<string, string>("MM","Przesunięcie międzymagazynowe") }
                };
            docsToAdd.ForEach(kv => DocumentTypes.Add(new DocumentType
            {
                Code = kv.Key,
                Name = kv.Value,

            }));


            return true;

        }

        #endregion Create Demo data
      
    }
}
