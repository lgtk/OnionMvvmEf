﻿using Core.Entities;
using Core.Enums;
using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class EfTaxRepository : EfRepositoryBase<Tax>, ITaxRepository
    {
        public EfTaxRepository(IContext context) : base(context)
        {
        }

        public IEnumerable<Tax> GetTaxes()
        {
            return context.Taxes.AsNoTracking().Where(x => x.Status != (int)DbModelStatusEnum.Deleted).ToList();
        }
    }
}
