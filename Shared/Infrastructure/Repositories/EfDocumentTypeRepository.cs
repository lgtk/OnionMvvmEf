﻿using Core.Entities;
using Core.Enums;
using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class EfDocumentTypeRepository : EfRepositoryBase<Client>, IDocumentTypeRepository
    {
     
        public EfDocumentTypeRepository(IContext context): base(context)
        {
          
        }

        public IEnumerable<DocumentType> GetAll()
        {
            return context.DocumentTypes.Where(x => x.Status != (int)DbModelStatusEnum.Deleted).AsNoTracking().ToList();
        }

        public DocumentType GetById(int id)
        {
            return context.DocumentTypes.FirstOrDefault(x => x.Id == id);
        }
    }
}
