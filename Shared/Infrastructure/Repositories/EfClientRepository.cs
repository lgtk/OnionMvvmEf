﻿using Core.Entities;
using Core.Enums;
using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class EfClientRepository : EfRepositoryBase<Client>, IClientRepository
    {
       

        public EfClientRepository(IContext context) : base(context)
        {
            
        }

        public IEnumerable<Client> GetAll()
        {
            return context.Clients.Where(x => x.Status != (int)DbModelStatusEnum.Deleted).AsNoTracking().ToList();
        }
    }
}
