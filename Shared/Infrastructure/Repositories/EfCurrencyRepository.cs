﻿using Core.Entities;
using Core.Enums;
using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class EfCurrencyRepository : EfRepositoryBase<Currency>, ICurrencyRepository
    {
        public EfCurrencyRepository(IContext context) : base(context)
        {
        }

        public IEnumerable<Currency> GetCurrencies()
        {
            return context.Currencies.Where(x => x.Status != (int)DbModelStatusEnum.Deleted).AsNoTracking().ToList();
        }
    }
}
