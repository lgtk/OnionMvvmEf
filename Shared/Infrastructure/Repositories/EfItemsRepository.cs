﻿using Core.Entities;
using Core.Enums;
using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class EfItemsRepository : EfRepositoryBase<Client>, IItemsRepository
    {


        public EfItemsRepository(IContext context) : base(context)
        {

        }
        public IEnumerable<Item> GetItems(int page = 0, int pageSize = 100)
        {
            return context.Items.AsNoTracking().Where(x => x.Status != (int)DbModelStatusEnum.Deleted).OrderBy(x => x.Id).Skip(0 * 100).Take(100).ToList();
        }

        public IEnumerable<Item> SearchItems(string text, int page = 0, int pageSize = 100)
        {
            return context.Items.AsNoTracking().Where(x => x.Status != (int)DbModelStatusEnum.Deleted && x.Name.ToUpper().Contains(text)).OrderBy(x => x.Id).Skip(0 * 100).Take(100).ToList();
        }
        public void Edit(Item item)
        {
            var dbItem = context.Items.FirstOrDefault(x => x.Id == item.Id);
            dbItem.Name = item.Name;
            dbItem.PrieceNet = item.PrieceNet;
            dbItem.TaxId = item.TaxId;
            dbItem.ModyfiDate = DateTime.UtcNow;
            dbItem.PrieceWithTax = item.PrieceWithTax;

            Save();


        }


        public void Remove(Item item)
        {
            var dbItem = context.Items.FirstOrDefault(x => x.Id == item.Id);
            dbItem.Status = (int)DbModelStatusEnum.Deleted;
            Save();


        }

        public void AddNew(Item item)
        {
            item.Id = 0;
            context.Items.Add(item);
            Save();
        }
    }

}
