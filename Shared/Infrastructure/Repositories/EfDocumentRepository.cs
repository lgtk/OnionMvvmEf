﻿using Core.Interfaces;
using Core.Entities;
using Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Core.Enums;

namespace Infrastructure.Repositories
{
    public class EfDocumentRepository : EfRepositoryBase<Client>, IDocumentRepository
    {

        public EfDocumentRepository(IContext context) : base(context)
        {

        }

        public Document GetDocument(int id)
        {
           return  context.Documents.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Document> GetPage(int page = 0, int pageSize = 100, SortingEnum sorting = SortingEnum.Desc)
        {
            var ret = context.Documents.Where(x => x.Status != (int)DbModelStatusEnum.Deleted).OrderBy(x => x.Id).Skip(page * pageSize).Take(pageSize).AsNoTracking().ToList();
            ret.ForEach(x =>
            {
                x.Items = context.DocumentItems.Where(di => di.DocumentId == x.Id && di.Status != (int)DbModelStatusEnum.Deleted).AsNoTracking().ToList();
            });
            return ret;
        }
        /// <summary>
        /// Only for demo sorting are only by id
        /// </summary>
        public IEnumerable<Document> SearchDocuments(Expression<Func<Document, bool>> search, int page = 0, int pageSize = 100, SortingEnum sorting = SortingEnum.Desc)
        {
            IEnumerable<Document> ret;

            if (sorting == SortingEnum.Asc)
            {
                ret = context.Documents.Where(search).OrderBy(x => x.Id).Skip(page * pageSize).Take(pageSize).AsNoTracking().ToList();
            }
            else
            {
                ret = context.Documents.Where(search).OrderByDescending(x => x.Id).Skip(page * pageSize).Take(pageSize).AsNoTracking().ToList();
            }

            return ret;
        }

        public void Remove(int id)
        {
            var doc = GetDocument(id);
            doc.Status = (int)DbModelStatusEnum.Deleted;

            context.SaveChanges();

        }

        public void RemoveDocumentItem(int documentItemId)
        {
            var documentItem = context.DocumentItems.FirstOrDefault(x => x.Id == documentItemId);
            documentItem.Status = (int)DbModelStatusEnum.Deleted;
            context.SaveChanges();
        }

        public void Add(Document document)
        {
            context.Documents.Add(document);
            context.SaveChanges();
        }

        public void AddItemsToDocument(IEnumerable<DocumentItem> items)
        {
            context.DocumentItems.AddRange(items);
            context.SaveChanges();
        }

        
    }
}
