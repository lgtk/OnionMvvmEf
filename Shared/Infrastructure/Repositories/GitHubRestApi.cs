﻿using Core.Entities;
using Core.Interfaces;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class GitHubRestApi : IRepositoryRestApi
    {
        private readonly string apiUrl = "https://api.github.com";
        public GitHubApiResponse GetRepositoryData(string user, string repositoryName)
        {
            var client = new RestClient(apiUrl);
            var request = new RestRequest("repos/{user}/{repositoryName}", Method.GET);
            request.AddUrlSegment("user", user);
            request.AddUrlSegment("repositoryName", repositoryName);
            var response = client.Get(request);
            var responseCOntent = response.Content;
            var ret = JsonConvert.DeserializeObject<GitHubApiResponse>(response.Content);
            return ret;

        }
    }
}
