﻿using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class EfRepositoryBase<T> : IRepositoryBase
    {
        protected readonly IContext context;
        public EfRepositoryBase(IContext context)
        {
            this.context = context;
        }

        public void Save()
        {
         
            context.SaveChanges();
        }

      

    }
}
