﻿using Core.Interfaces;
using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Logging
{
    public class NLogLogger : Core.Interfaces.ILogger
    {
        NLog.ILogger nLog;
        private readonly string loggerName;
     
        public NLogLogger(string loggerName)
        {
            this.loggerName = loggerName;
           
            nLog = LogManager.GetLogger(loggerName);

        }

      

        public void Debug(string msg)
        {
            nLog.Debug(msg);
        }

        public void Error(Exception ex, string msg)
        {
            nLog.Error(ex, msg);
        }

        public void FatalError(Exception ex, string msg)
        {
            nLog.Fatal(ex,msg);
        }

        public void Info(string msg)
        {
            nLog.Info(msg);
        }

        public void Trace(string msg)
        {
            nLog.Trace(msg);
        }
    }
}
