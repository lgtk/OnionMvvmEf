﻿using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class AppConfiguration : IAppConfiguration
    {
        public AppConfiguration()
        {

        }

        public string ConnectionString { get; set; }
    }
}
