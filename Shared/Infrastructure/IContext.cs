﻿using System.Data.Entity;
using Core.Entities;

namespace Infrastructure
{
    public interface IContext
    {
       
        DbSet<Client> Clients { get; set; }
        DbSet<DocumentItem> DocumentItems { get; set; }
        DbSet<Document> Documents { get; set; }
        DbSet<Item> Items { get; set; }
        DbSet<Tax> Taxes { get; set; }
        DbSet<Currency> Currencies { get; set; }
        DbSet<DocumentType> DocumentTypes { get; set; }
        int SaveChanges();
    }
}