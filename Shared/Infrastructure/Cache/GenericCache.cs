﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Cache
{
    /// <summary>
    /// We can store dictionaries like DocTypes, Currencies or Tax Values in cache because they not changing usually.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GenericCache<T> where T : class
    {
        private readonly string cacheKey;
        private readonly MemoryCache cache;

        public GenericCache()
        {
            cacheKey = typeof(T).Name;
            cache = MemoryCache.Default;
        }

        public IEnumerable<T> Get()
        {
            var ci = cache.GetCacheItem(cacheKey);
            if(ci?.Value == null)
            {
                return null;
            }
            return ci.Value as IEnumerable<T>;
        }

        public IEnumerable<T> GetOrAdd(Func<IEnumerable<T>> fetchItems, int cacheExpirationMinutes = 60)
        {
            var ci = cache.GetCacheItem(cacheKey);
            IEnumerable<T> items;
            if (ci?.Value == null)
            {
                items = fetchItems();
                Add(items, cacheExpirationMinutes);
            }
            else
            {
                items = ci.Value as IEnumerable<T>;
            }
            return items;
        }

        public void Add(IEnumerable<T> items, int cacheExpirationMinutes)
        {
            var ci = cache.Contains(cacheKey);
            if (ci)
            {
                return;
            }
            cache.Add(cacheKey, items, new CacheItemPolicy { AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddMinutes(cacheExpirationMinutes)) });
        }
    }
}
