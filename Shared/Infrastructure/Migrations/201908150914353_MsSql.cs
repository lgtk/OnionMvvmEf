namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MsSql : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        Description = c.String(),
                        SaveDate = c.DateTime(nullable: false),
                        ModyfiDate = c.DateTime(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        DocumentNumber = c.String(),
                        ClientId = c.Int(nullable: false),
                        TotalPrieceNet = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SaveDate = c.DateTime(nullable: false),
                        ModyfiDate = c.DateTime(),
                        Status = c.Int(nullable: false),
                        CurrencyId = c.Int(nullable: false),
                        DocumentTypeId = c.Int(nullable: false),
                        TotalPrieceWithTax = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .ForeignKey("dbo.Currencies", t => t.CurrencyId, cascadeDelete: true)
                .ForeignKey("dbo.DocumentTypes", t => t.DocumentTypeId, cascadeDelete: true)
                .Index(t => t.ClientId)
                .Index(t => t.CurrencyId)
                .Index(t => t.DocumentTypeId);
            
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SaveDate = c.DateTime(nullable: false),
                        ModyfiDate = c.DateTime(),
                        Code = c.String(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DocumentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        SaveDate = c.DateTime(nullable: false),
                        ModyfiDate = c.DateTime(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DocumentItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentId = c.Int(nullable: false),
                        ItemId = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                        PrieceNet = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PrieceWithTax = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SaveDate = c.DateTime(nullable: false),
                        ModyfiDate = c.DateTime(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Documents", t => t.DocumentId, cascadeDelete: true)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .Index(t => t.DocumentId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PrieceNet = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PrieceWithTax = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TaxId = c.Int(nullable: false),
                        SaveDate = c.DateTime(nullable: false),
                        ModyfiDate = c.DateTime(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Taxes", t => t.TaxId, cascadeDelete: true)
                .Index(t => t.TaxId);
            
            CreateTable(
                "dbo.Taxes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Value = c.Int(nullable: false),
                        SaveDate = c.DateTime(nullable: false),
                        ModyfiDate = c.DateTime(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DocumentItems", "ItemId", "dbo.Items");
            DropForeignKey("dbo.Items", "TaxId", "dbo.Taxes");
            DropForeignKey("dbo.DocumentItems", "DocumentId", "dbo.Documents");
            DropForeignKey("dbo.Documents", "DocumentTypeId", "dbo.DocumentTypes");
            DropForeignKey("dbo.Documents", "CurrencyId", "dbo.Currencies");
            DropForeignKey("dbo.Documents", "ClientId", "dbo.Clients");
            DropIndex("dbo.Items", new[] { "TaxId" });
            DropIndex("dbo.DocumentItems", new[] { "ItemId" });
            DropIndex("dbo.DocumentItems", new[] { "DocumentId" });
            DropIndex("dbo.Documents", new[] { "DocumentTypeId" });
            DropIndex("dbo.Documents", new[] { "CurrencyId" });
            DropIndex("dbo.Documents", new[] { "ClientId" });
            DropTable("dbo.Taxes");
            DropTable("dbo.Items");
            DropTable("dbo.DocumentItems");
            DropTable("dbo.DocumentTypes");
            DropTable("dbo.Currencies");
            DropTable("dbo.Documents");
            DropTable("dbo.Clients");
        }
    }
}
