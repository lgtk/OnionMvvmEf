﻿using Core.Entities;
using Core.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using Tests.FakeImplementations;
using Tests.FakeImplementations.FakeRepos;

namespace Services.Tests
{
    [TestClass()]
    public class DocumentsServiceTests
    {

        private IDocumentsService BuidService(int items, FakeRepositoryMode mode)
        {

            IDocumentRepository docs = new FakeDocumentsRepository(200, mode);
            IDocumentTypeRepository dt = new FakeDocumentTypeRepository(200, mode);
            ILoggerFactory loggerFactory = new FakeLoggerFactory();
            return new DocumentsService(docs, dt, loggerFactory);
        }


        [TestMethod()]
        public async Task AddDocumentAsyncTest()
        {
            var normal = BuidService(100, FakeRepositoryMode.NormalWorking);
            var throwing = BuidService(100, FakeRepositoryMode.ThrowsExceptions);
            var nulls = BuidService(100, FakeRepositoryMode.ReturnNulls);

            var normalResult = await normal.AddDocumentAsync(new Document(), new List<DocumentItem>());
            var throwingResult = await throwing.AddDocumentAsync(new Document(), new List<DocumentItem>());
            var nullsResult = await nulls.AddDocumentAsync(new Document(), new List<DocumentItem>());
            Assert.IsTrue(normalResult.State == Enums.ServiceCallState.Success, "normal err");
            Assert.IsTrue(throwingResult.State == Enums.ServiceCallState.Error, "throwing err");
            Assert.IsTrue(nullsResult.State == Enums.ServiceCallState.Success, "nulls err");

            var normalResultNull = await normal.AddDocumentAsync(null, null);
            var throwingResultNull = await throwing.AddDocumentAsync(null, null);
            var nullsResultNull = await nulls.AddDocumentAsync(null, null);
            Assert.IsTrue(normalResultNull.State == Enums.ServiceCallState.Error, "normal err");
            Assert.IsTrue(throwingResultNull.State == Enums.ServiceCallState.Error, "throwing err");
            Assert.IsTrue(nullsResultNull.State == Enums.ServiceCallState.Error, "nulls err");
        }

        [TestMethod()]
        public void AddDocumentTest()
        {
            var normal = BuidService(100, FakeRepositoryMode.NormalWorking);
            var throwing = BuidService(100, FakeRepositoryMode.ThrowsExceptions);
            var nulls = BuidService(100, FakeRepositoryMode.ReturnNulls);

            var normalResult = normal.AddDocument(new Document(), new List<DocumentItem>());
            var throwingResult = throwing.AddDocument(new Document(), new List<DocumentItem>());
            var nullsResult = nulls.AddDocument(new Document(), new List<DocumentItem>());
            Assert.IsTrue(normalResult.State == Enums.ServiceCallState.Success, "normal err");
            Assert.IsTrue(throwingResult.State == Enums.ServiceCallState.Error, "throwing err");
            Assert.IsTrue(nullsResult.State == Enums.ServiceCallState.Success, "nulls err");

            var normalResultNull = normal.AddDocument(null, null);
            var throwingResultNull = throwing.AddDocument(null, null);
            var nullsResultNull = nulls.AddDocument(null, null);

            Assert.IsTrue(normalResultNull.State == Enums.ServiceCallState.Error, "normal err");
            Assert.IsTrue(throwingResultNull.State == Enums.ServiceCallState.Error, "throwing err");
            Assert.IsTrue(nullsResultNull.State == Enums.ServiceCallState.Error, "nulls err");
        }

        [TestMethod()]
        public void UpdateDocumentTest()
        {
            var normal = BuidService(100, FakeRepositoryMode.NormalWorking);
            var throwing = BuidService(100, FakeRepositoryMode.ThrowsExceptions);
            var nulls = BuidService(100, FakeRepositoryMode.ReturnNulls);

            var normalResult = normal.UpdateDocument(new Document(), new List<DocumentItem>());
            var throwingResult = throwing.UpdateDocument(new Document(), new List<DocumentItem>());
            var nullsResult = nulls.UpdateDocument(new Document(), new List<DocumentItem>());
            Assert.IsTrue(normalResult.State == Enums.ServiceCallState.Success, "normal err");
            Assert.IsTrue(throwingResult.State == Enums.ServiceCallState.Error, "throwing err");
            Assert.IsTrue(nullsResult.State == Enums.ServiceCallState.Error, "nulls err");


            var normalResultNull = normal.UpdateDocument(null, null);
            var throwingResultNull = throwing.UpdateDocument(null, null);
            var nullsResultNull = nulls.UpdateDocument(null, null);
            Assert.IsTrue(normalResultNull.State == Enums.ServiceCallState.Error, "normal err");
            Assert.IsTrue(throwingResultNull.State == Enums.ServiceCallState.Error, "throwing err");
            Assert.IsTrue(nullsResultNull.State == Enums.ServiceCallState.Error, "nulls err");
        }

        [TestMethod()]
        public async Task GetDocumentTypesAsyncTest()
        {
            var normal = BuidService(100, FakeRepositoryMode.NormalWorking);
            var throwing = BuidService(100, FakeRepositoryMode.ThrowsExceptions);
            var nulls = BuidService(100, FakeRepositoryMode.ReturnNulls);
            MemoryCache.Default.Remove(typeof(DocumentType).Name);
            var normalResult = await normal.GetDocumentTypesAsync();
            MemoryCache.Default.Remove(typeof(DocumentType).Name);
            var throwingResult = await throwing.GetDocumentTypesAsync();
            MemoryCache.Default.Remove(typeof(DocumentType).Name);
            var nullsResult = await nulls.GetDocumentTypesAsync();
            Assert.IsTrue(normalResult.State == Enums.ServiceCallState.Success, "normal err");
            Assert.IsTrue(throwingResult.State == Enums.ServiceCallState.Error, "throwing err");
            Assert.IsTrue(nullsResult.State == Enums.ServiceCallState.Error, "nulls err");
        }

        [TestMethod()]
        public void GetDocumentTypesTest()
        {
            var normal = BuidService(100, FakeRepositoryMode.NormalWorking);
            var throwing = BuidService(100, FakeRepositoryMode.ThrowsExceptions);
            var nulls = BuidService(100, FakeRepositoryMode.ReturnNulls);
            MemoryCache.Default.Remove(typeof(DocumentType).Name);
            var normalResult = normal.GetDocumentTypes();
            MemoryCache.Default.Remove(typeof(DocumentType).Name);
            var throwingResult = throwing.GetDocumentTypes();
            MemoryCache.Default.Remove(typeof(DocumentType).Name);
            var nullsResult = nulls.GetDocumentTypes();
            Assert.IsTrue(normalResult.State == Enums.ServiceCallState.Success, "normal err");
            Assert.IsTrue(throwingResult.State == Enums.ServiceCallState.Error, "throwing err");
            Assert.IsTrue(nullsResult.State == Enums.ServiceCallState.Error, "nulls err");
        }

        [TestMethod()]
        public void GetDocumentsTest()
        {
            var normal = BuidService(100, FakeRepositoryMode.NormalWorking);
            var throwing = BuidService(100, FakeRepositoryMode.ThrowsExceptions);
            var nulls = BuidService(100, FakeRepositoryMode.ReturnNulls);

            var normalResult = normal.GetDocuments();
            var throwingResult = throwing.GetDocuments();
            var nullsResult = nulls.GetDocuments();
            Assert.IsTrue(normalResult.State == Enums.ServiceCallState.Success, "normal err");
            Assert.IsTrue(throwingResult.State == Enums.ServiceCallState.Error, "throwing err");
            Assert.IsTrue(nullsResult.State == Enums.ServiceCallState.Error, "nulls err");
        }

        [TestMethod()]
        public void SearchDocumentsTest()
        {
            var normal = BuidService(100, FakeRepositoryMode.NormalWorking);
            var throwing = BuidService(100, FakeRepositoryMode.ThrowsExceptions);
            var nulls = BuidService(100, FakeRepositoryMode.ReturnNulls);

            var normalResult = normal.SearchDocuments("test");
            var throwingResult = throwing.SearchDocuments("test");
            var nullsResult = nulls.SearchDocuments("test");
            Assert.IsTrue(normalResult.State == Enums.ServiceCallState.Success, "normal err");
            Assert.IsTrue(throwingResult.State == Enums.ServiceCallState.Error, "throwing err");
            Assert.IsTrue(nullsResult.State == Enums.ServiceCallState.Error, "nulls err");
        }

        [TestMethod()]
        public async Task SearchDocumentsAsyncTest()
        {
            var normal = BuidService(100, FakeRepositoryMode.NormalWorking);
            var throwing = BuidService(100, FakeRepositoryMode.ThrowsExceptions);
            var nulls = BuidService(100, FakeRepositoryMode.ReturnNulls);

            var normalResult = await normal.SearchDocumentsAsync("test");
            var throwingResult = await throwing.SearchDocumentsAsync("test");
            var nullsResult = await nulls.SearchDocumentsAsync("test");
            Assert.IsTrue(normalResult.State == Enums.ServiceCallState.Success, "normal err");
            Assert.IsTrue(throwingResult.State == Enums.ServiceCallState.Error, "throwing err");
            Assert.IsTrue(nullsResult.State == Enums.ServiceCallState.Error, "nulls err");
        }

        [TestMethod()]
        public async Task GetDocumentsAsyncTest()
        {
            var normal = BuidService(100, FakeRepositoryMode.NormalWorking);
            var throwing = BuidService(100, FakeRepositoryMode.ThrowsExceptions);
            var nulls = BuidService(100, FakeRepositoryMode.ReturnNulls);

            var normalResult = await normal.GetDocumentsAsync();
            var throwingResult = await throwing.GetDocumentsAsync();
            var nullsResult = await nulls.GetDocumentsAsync();
            Assert.IsTrue(normalResult.State == Enums.ServiceCallState.Success, "normal err");
            Assert.IsTrue(throwingResult.State == Enums.ServiceCallState.Error, "throwing err");
            Assert.IsTrue(nullsResult.State == Enums.ServiceCallState.Error, "nulls err");
        }

        [TestMethod()]
        public void GenerateDocumentNumberTest()
        {
            var normal = BuidService(100, FakeRepositoryMode.NormalWorking);
            var throwing = BuidService(100, FakeRepositoryMode.ThrowsExceptions);
            var nulls = BuidService(100, FakeRepositoryMode.ReturnNulls);

            var normalResult = normal.GenerateDocumentNumber(new DocumentType { Code = "CC", Name = "CC" }, new Client { Code = "XXXX" });
            var throwingResult = throwing.GenerateDocumentNumber(new DocumentType { Code = "CC", Name = "CC" }, new Client { Code = "XXXX" });
            var nullsResult = nulls.GenerateDocumentNumber(new DocumentType { Code = "CC", Name = "CC" }, new Client { Code = "XXXX" });
            Assert.IsTrue(normalResult.State == Enums.ServiceCallState.Success, "normal err");
            Assert.IsTrue(throwingResult.State == Enums.ServiceCallState.Success, "throwing err");
            Assert.IsTrue(nullsResult.State == Enums.ServiceCallState.Success, "nulls err");


            var normalResultNull = normal.GenerateDocumentNumber(null,null);
            var throwingResultNull = throwing.GenerateDocumentNumber(null, null);
            var nullsResultNull = nulls.GenerateDocumentNumber(null, null);
            Assert.IsTrue(normalResultNull.State == Enums.ServiceCallState.Error, "normal err");
            Assert.IsTrue(throwingResultNull.State == Enums.ServiceCallState.Error, "throwing err");
            Assert.IsTrue(nullsResultNull.State == Enums.ServiceCallState.Error, "nulls err");

        }

        [TestMethod()]
        public async Task RemoveAsyncTest()
        {
            var normal = BuidService(100, FakeRepositoryMode.NormalWorking);
            var throwing = BuidService(100, FakeRepositoryMode.ThrowsExceptions);
            var nulls = BuidService(100, FakeRepositoryMode.ReturnNulls);
            var first = normal.GetDocuments()?.Result?.FirstOrDefault();
            var normalResult = await normal.RemoveAsync(first);
            var throwingResult = await throwing.RemoveAsync(first);
            var nullsResult = await nulls.RemoveAsync(first);
            Assert.IsTrue(normalResult.State == Enums.ServiceCallState.Success, "normal err");
            Assert.IsTrue(throwingResult.State == Enums.ServiceCallState.Error, "throwing err");
            Assert.IsTrue(nullsResult.State == Enums.ServiceCallState.Error, "nulls err");
        }

        [TestMethod()]
        public void RemoveTest()
        {
            var normal = BuidService(100, FakeRepositoryMode.NormalWorking);
            var throwing = BuidService(100, FakeRepositoryMode.ThrowsExceptions);
            var nulls = BuidService(100, FakeRepositoryMode.ReturnNulls);
            var first = normal.GetDocuments()?.Result?.FirstOrDefault();


            var normalResult =  normal.Remove(first);
            var throwingResult =  throwing.Remove(first);
            var nullsResult =  nulls.Remove(first);
            Assert.IsTrue(normalResult.State == Enums.ServiceCallState.Success, "normal err");
            Assert.IsTrue(throwingResult.State == Enums.ServiceCallState.Error, "throwing err");
            Assert.IsTrue(nullsResult.State == Enums.ServiceCallState.Error, "nulls err");

            Document doc = null;
            var normalResultNull = normal.Remove(doc);
            var throwingResultNull = throwing.Remove(doc);
            var nullsResultNull = nulls.Remove(doc);
            Assert.IsTrue(normalResultNull.State == Enums.ServiceCallState.Error, "normal err 2");
            Assert.IsTrue(throwingResultNull.State == Enums.ServiceCallState.Error, "throwing err 2");
            Assert.IsTrue(nullsResultNull.State == Enums.ServiceCallState.Error, "nulls err 2");
        }


        [TestMethod()]
        public void RemoveItemTest()
        {
            var normal = BuidService(100, FakeRepositoryMode.NormalWorking);
            var throwing = BuidService(100, FakeRepositoryMode.ThrowsExceptions);
         
        


            var normalResult = normal.Remove(new DocumentItem() { Id = -1 });
            var throwingResult = throwing.Remove(new DocumentItem() { Id = -1 });
 
            Assert.IsTrue(normalResult.State == Enums.ServiceCallState.Success, "normal err");
            Assert.IsTrue(throwingResult.State == Enums.ServiceCallState.Error, "throwing err");
        

            DocumentItem doc = null;
            var normalResultNull = normal.Remove(doc);
            var throwingResultNull = throwing.Remove(doc);
         
            Assert.IsTrue(normalResultNull.State == Enums.ServiceCallState.Error, "normal err");
            Assert.IsTrue(throwingResultNull.State == Enums.ServiceCallState.Error, "throwing err");
      
        }

        [TestMethod()]
        public async Task  RemoveItemAsyncTest()
        {
            var normal = BuidService(100, FakeRepositoryMode.NormalWorking);
            var throwing = BuidService(100, FakeRepositoryMode.ThrowsExceptions);
            var nulls = BuidService(100, FakeRepositoryMode.ReturnNulls);
            var first = normal.GetDocuments()?.Result?.FirstOrDefault();


            var normalResult = await normal.RemoveAsync(new DocumentItem());
            var throwingResult = await throwing.RemoveAsync(new DocumentItem() { Id = 1});
    
            Assert.IsTrue(normalResult.State == Enums.ServiceCallState.Success, "normal err");
            Assert.IsTrue(throwingResult.State == Enums.ServiceCallState.Error, "throwing err");
        

            DocumentItem doc = null;
            var normalResultNull = await normal.RemoveAsync(doc);
            var throwingResultNull = await throwing.RemoveAsync(doc);
            var nullsResultNull = await nulls.RemoveAsync(doc);
            Assert.IsTrue(normalResultNull.State == Enums.ServiceCallState.Error, "normal err");
            Assert.IsTrue(throwingResultNull.State == Enums.ServiceCallState.Error, "throwing err");
            Assert.IsTrue(nullsResultNull.State == Enums.ServiceCallState.Error, "nulls err");
        }

    }
}