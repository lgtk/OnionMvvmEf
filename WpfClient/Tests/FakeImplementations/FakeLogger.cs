﻿using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.FakeImplementations
{
    internal class FakeLoggerFactory : ILoggerFactory
    {
       
        public ILogger CreateLogger(string loggerName)
        {
            return new FakeLogger(loggerName);
        }
    }

    internal class FakeLogger : ILogger
    {
        private readonly string loggerName;

        public FakeLogger(string loggerName)
        {
            this.loggerName = loggerName;
        }
        public void Debug(string msg)
        {
            Console.WriteLine($"DEBUG: {msg}");
        }

        public void Error(Exception ex, string msg)
        {
            Console.WriteLine($"ERROR: {msg} {ex.Message}");
        }

        public void FatalError(Exception ex, string msg)
        {
            Console.WriteLine($"FATAL: {msg} {ex.Message}");
        }

        public void Info(string msg)
        {
            Console.WriteLine($"INFO: {msg}");
        }

        public void Trace(string msg)
        {
            Console.WriteLine($"TRACE: {msg}");
        }
    }

}
