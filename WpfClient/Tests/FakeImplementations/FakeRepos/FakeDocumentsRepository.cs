﻿using Core.Entities;
using Core.Interfaces;
using Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Tests.FakeImplementations.FakeRepos
{
   
    internal class FakeDocumentsRepository : IDocumentRepository
    {
        private readonly IList<Document> items;
        private readonly FakeRepositoryMode state;

        public FakeDocumentsRepository(int fakeItemsCount, FakeRepositoryMode state)
        {
            items = state == FakeRepositoryMode.NormalWorking ?  GenerateFakeDocs(fakeItemsCount) : GenerateNull(fakeItemsCount);
            this.state = state;
        }
        public void Add(Document document)
        {
            if (state == FakeRepositoryMode.ThrowsExceptions)
            {
                throw new Exception("Err in Add");
            }
            items.Add(document);
        }

        public void AddItemsToDocument(IEnumerable<DocumentItem> items)
        {
            if (state == FakeRepositoryMode.ThrowsExceptions)
            {
                throw new Exception("Err in AddItemsToDocument");
            }
        }

        public Document GetDocument(int id)
        {
            if (state == FakeRepositoryMode.ThrowsExceptions)
            {
                throw new Exception("Err in GetDocument");
            }
            else if (state == FakeRepositoryMode.ReturnNulls)
            {
                return null;
            }
            return new Document { Id = id };
        }

        public IEnumerable<Document> GetPage(int page = 0, int pageSize = 100, SortingEnum sorting = SortingEnum.Desc)
        {
            if (state == FakeRepositoryMode.ThrowsExceptions)
            {
                throw new Exception("Err in GetDocument");
            }
            else if (state == FakeRepositoryMode.ReturnNulls)
            {
                return null;
            }
            return items.Skip(page* pageSize).Take(pageSize);
        }

        public void RemoveDocumentItem(int documentItemId)
        {
            if (state == FakeRepositoryMode.ThrowsExceptions)
            {
                throw new Exception("Err in GetDocument");
            }
        }

        public void Save()
        {
            if (state == FakeRepositoryMode.ThrowsExceptions)
            {
                throw new Exception("Err in GetDocument");
            }
        }

        public IEnumerable<Document> SearchDocuments(Expression<Func<Document, bool>> search, int page = 0, int pageSize = 0, SortingEnum sorting = SortingEnum.Desc)
        {
            if (state == FakeRepositoryMode.ThrowsExceptions)
            {
                throw new Exception("Err in GetDocument");
            }
            else if (state == FakeRepositoryMode.ReturnNulls)
            {
                return null;
            }
            return items.ToList().Where(search.Compile()).Skip(page * pageSize).Take(pageSize);
        }

        private IList<Document> GenerateFakeDocs(int fakeItemsCount)
        {
            var list = new List<Document>();

            for (int i = 0; i < fakeItemsCount;  i++)
            {
                list.Add(new Document
                {
                    Id = i,
                    DocumentNumber = $"NUM/{i}",
                    Items = new List<DocumentItem> {
                         new DocumentItem{ Id = 1 }
                    }
                });
            }
            return list;
        }

        private IList<Document> GenerateNull(int fakeItemsCount)
        {
            var list = new List<Document>();

            for (int i = 0; i < fakeItemsCount; i++)
            {
                list.Add(null);
            }
            return list;
        }
    }
}
