﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.FakeImplementations.FakeRepos
{
    internal enum FakeRepositoryMode
    {
        NormalWorking,
        NormalButUseNulledList,
        ThrowsExceptions,
        ReturnNulls
    }
}
