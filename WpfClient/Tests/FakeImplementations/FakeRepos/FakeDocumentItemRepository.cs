﻿using Core.Entities;
using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.FakeImplementations.FakeRepos
{
    internal class FakeDocumentTypeRepository : IDocumentTypeRepository
    {
        private readonly IList<DocumentType> items;
        private readonly FakeRepositoryMode state;

        public FakeDocumentTypeRepository(int fakeItemsCount, FakeRepositoryMode state)
        {
            items = state == FakeRepositoryMode.NormalWorking ? GenerateFakeTypes(fakeItemsCount) : GenerateNull(fakeItemsCount);
            this.state = state;
        }

        public IEnumerable<DocumentType> GetAll()
        {
            if (state == FakeRepositoryMode.ThrowsExceptions)
            {
                throw new Exception("Err in GetDocument");
            }
            else if (state == FakeRepositoryMode.ReturnNulls)
            {
                return null;
            }
            return items;
        }

        public DocumentType GetById(int id)
        {
            if (state == FakeRepositoryMode.ThrowsExceptions)
            {
                throw new Exception("Err in GetDocument");
            }
            else if (state == FakeRepositoryMode.ReturnNulls)
            {
                return null;
            }
            return items.FirstOrDefault(x => x.Id == id);
        }

        public void Save()
        {
            if (state == FakeRepositoryMode.ThrowsExceptions)
            {
                throw new Exception("Err in GetDocument");
            }

        }

        private IList<DocumentType> GenerateFakeTypes(int fakeItemsCount)
        {
            var list = new List<DocumentType>();

            for (int i = 0; i < fakeItemsCount; i++)
            {
                list.Add(new DocumentType
                {
                    Id = i,
                    Name = $"TYPE{i}",
                    Code = i.ToString()
                });
            }
            return list;
        }

        private IList<DocumentType> GenerateNull(int fakeItemsCount)
        {
            var list = new List<DocumentType>();

            for (int i = 0; i < fakeItemsCount; i++)
            {
                list.Add(null);
            }
            return list;
        }
    }
}
