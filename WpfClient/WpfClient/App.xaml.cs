﻿using Autofac;
using Core.Interfaces;
using Infrastructure;
using Infrastructure.Logging;
using Infrastructure.Repositories;
using Microsoft.Extensions.Configuration;
using Services;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WpfClient.Session;
using WpfClient.ViewModel;


namespace WpfClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        public IContainer Container { get; set; }
        protected override void OnStartup(StartupEventArgs e)
        {
          
            BuildContainer();
        }

        private void BuildContainer()
        {
            var builder = new ContainerBuilder();
            var conn = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            builder.RegisterInstance(new AppConfiguration { ConnectionString = conn }).As<IAppConfiguration>();
            builder.RegisterInstance(new SharedSession());
            builder.RegisterType<MsSqlContext>().As<IContext>();
            builder.RegisterType<NLogLoggerFactory>().As<ILoggerFactory>();
            builder.RegisterType<GitHubRestApi>().As<IRepositoryRestApi>();
            builder.RegisterType<EfDocumentTypeRepository>().As<IDocumentTypeRepository>();
            builder.RegisterType<EfDocumentRepository>().As<IDocumentRepository>();
            builder.RegisterType<EfClientRepository>().As<IClientRepository>();
            builder.RegisterType<EfItemsRepository>().As<IItemsRepository>();
            builder.RegisterType<EfCurrencyRepository>().As<ICurrencyRepository>();
            builder.RegisterType<EfTaxRepository>().As<ITaxRepository>();
            builder.RegisterType<CurrencyService>().As<ICurrencyService>();
            builder.RegisterType<ClientsService>().As<IClientsService>();
            builder.RegisterType<GitHubService>().As<IGitHubService>();
            builder.RegisterType<DocumentsService>().As<IDocumentsService>();
            builder.RegisterType<ItemService>().As<IItemService>();
            builder.RegisterType<TaxService>().As<ITaxService>();
            builder.RegisterType<MainWindow>();
            builder.RegisterType<MainViewModel>();
            builder.RegisterType<DocumentsViewModel>();
            builder.RegisterType<ManageDocumentViewModel>();
            builder.RegisterType<ManageProductViewModel>();
            Container = builder.Build();
        }
    }
}
