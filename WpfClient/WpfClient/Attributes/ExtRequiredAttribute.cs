﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient.Attributes
{
    public class ExtRequiredAttribute : RequiredAttribute
    {
        public bool IsNumericStr { get; set; } = false;
    }
}
