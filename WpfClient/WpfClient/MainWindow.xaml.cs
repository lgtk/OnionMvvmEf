﻿using FirstFloor.ModernUI.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfClient.Messages;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ModernWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            Messenger.Default.Register<UiActionMessage>(this, OnOpenNewDocumentWindow);
            Messenger.Default.Register<ShowDialogMessage>(this, OnDialogMessage);
            Title = "Wpf Client";
            Messenger.Default.Register<UpdateWindowTitle>(this, OnUpdateTitle);
        }

        private void OnUpdateTitle(UpdateWindowTitle obj)
        {
            Dispatcher.Invoke(() => { Title = obj.NewTitle; });

        }

        private void OnDialogMessage(ShowDialogMessage obj)
        {
            ModernDialog.ShowMessage("Uwaga", obj.Message, MessageBoxButton.OK);
        }

        private void OnOpenNewDocumentWindow(UiActionMessage msg)
        {
            switch (msg)
            {
                case UiActionMessage.Default:
                    break;
                case UiActionMessage.RefreshDocumentsList:
                    break;
                case UiActionMessage.GoToManageDocument:
                    Frame.Source = new Uri(@"/View/ManageDocumentView.xaml", UriKind.RelativeOrAbsolute);

                    break;
                case UiActionMessage.ClosePopUpWindow:
                    break;
                case UiActionMessage.BackToDocumentsList:
                    Frame.Source = new Uri(@"/View/DocumentsView.xaml", UriKind.RelativeOrAbsolute);
                    break;
                default:
                    break;
            }


        }
    }
}
