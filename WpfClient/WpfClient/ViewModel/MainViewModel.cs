using System;
using System.Threading;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Services.Interfaces;
using WpfClient.Messages;

namespace WpfClient.ViewModel
{

    public class MainViewModel : ViewModel
    {
        private readonly IGitHubService service;
        private readonly Timer timer;
        public MainViewModel(IGitHubService service)
        {
            Title = "Main Window";
            this.service = service;
            Task.Run(() => OnLoadRepoData());
            timer = new Timer(delegate { OnLoadRepoData(); });
            timer.Change(10000, 1000 * 60);
        }

        private async void OnLoadRepoData()
        {
            var data = await service.GetRepositoryDataAsync("lgtk", "OnionMvvmEf");
            var title = string.Empty;
            if (data.State == Services.Enums.ServiceCallState.Error)
            {
                title = $"Wpf Client - {data.Message}";
            }
            else
            {
                title = $"Wpf Client - Owner: {data.Result.Owner.Name}, Repo name: {data.Result.Name}, Create Date: {data.Result.CreatedAt}, Stars: {data.Result.Stars}";
            }
            MessengerInstance.Send(new UpdateWindowTitle(title));
        }


    }
}