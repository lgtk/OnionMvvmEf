﻿using Core.Entities;
using Core.Interfaces;
using FirstFloor.ModernUI.Presentation;
using Services;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WpfClient.Attributes;
using WpfClient.Messages;
using WpfClient.Session;
using WpfClient.View;

namespace WpfClient.ViewModel
{
    public class ManageDocumentViewModel : ViewModel
    {
        private readonly IClientsService clientService;
        private readonly ICurrencyService currencyService;
        private readonly IDocumentsService documentsService;
        private readonly SharedSession session;
        private string documentNumber;
        private Document documentToEdit;
        private Window popUpWindow = null;

        #region .ctor

        public ManageDocumentViewModel(IDocumentsService service, IClientsService clientService, ICurrencyService currencyService, SharedSession session)
        {
            this.documentsService = service;
            this.session = session;
            this.currencyService = currencyService;
            this.clientService = clientService;
            var docTypes = service.GetDocumentTypes();
            if (docTypes.State == Services.Enums.ServiceCallState.Error)
            {
                MessengerInstance.Send(new ShowDialogMessage(docTypes.Message));
                return;
            }
            foreach (var docType in docTypes.Result)
            {
                DocumentTypes.Add(docType);
            }

            var clients = clientService.GetClients();
            if (clients.State == Services.Enums.ServiceCallState.Error)
            {
                MessengerInstance.Send(new ShowDialogMessage(clients.Message));
                return;
            }
            foreach (var client in clients.Result)
            {
                Clients.Add(client);
            }
            var currencies = currencyService.GetCurrencies();
            if (currencies.State == Services.Enums.ServiceCallState.Error)
            {
                MessengerInstance.Send(new ShowDialogMessage(currencies.Message));
                return;
            }

            foreach (var c in currencies.Result)
            {
                Currencies.Add(c);
            }
            MessengerInstance.Register<NewProductToDocumentMessage>(this, OnReciveMessage);
            NumberEditable = true;
            if (session.SelectedDocument != null)
            {
                OnProductToEditMessage(session.SelectedDocument);
            }

        }

        private void OnProductToEditMessage(Document obj)
        {
            documentToEdit = obj;
            SelectedDocumentType = documentTypes.FirstOrDefault(x => x.Id == documentToEdit.DocumentTypeId);
            SelectedClient = clients.FirstOrDefault(x => x.Id == documentToEdit.ClientId);
            SelectedCurrency = currencies.FirstOrDefault(x => x.Id == documentToEdit.CurrencyId);
            DocumentNumber = documentToEdit.DocumentNumber;
            foreach (var item in documentToEdit.Items)
            {
                DocumentItems.Add(item);
            }
            NumberEditable = false;
        }

        #endregion


        #region BackCommand


        private RelayCommand backCommand;

        /// <summary>
        /// Gets the MyCommand.
        /// </summary>
        public RelayCommand BackCommand
        {
            get
            {
                return backCommand
                    ?? (backCommand = new RelayCommand(
                    OnBackCommand));
            }
        }

        private void OnBackCommand(object o)
        {
            MessengerInstance.Send(UiActionMessage.BackToDocumentsList);
        }
        #endregion

        #region SaveCommand

        private RelayCommand saveCommand;


        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new RelayCommand(async obj => await OnSave(obj), CanExecureSaveDocument);
                }
                return saveCommand;
            }
        }
        private bool CanExecureSaveDocument(object obj)
        {
            return  SelectedClient != null && SelectedDocumentType != null && SelectedCurrency != null && DocumentNumber != string.Empty && DocumentItems.Any();
        }

        private async Task OnSave(object obj)
        {
            if (documentToEdit != null)
            {
                documentToEdit.TotalPrieceNet = DocumentItems.Sum(x => x.PrieceNet);
                documentToEdit.TotalPrieceWithTax = DocumentItems.Sum(x => x.PrieceWithTax);
                documentToEdit.CurrencyId = SelectedCurrency.Id;
                documentsService.UpdateDocument(documentToEdit, DocumentItems.ToList());

            }
            else

            {
                await documentsService.AddDocumentAsync(new Document
                {

                    ClientId = SelectedClient.Id,
                    DocumentTypeId = SelectedDocumentType.Id,
                    DocumentNumber = documentNumber,
                    TotalPrieceNet = DocumentItems.Sum(x => x.PrieceNet),
                    TotalPrieceWithTax = DocumentItems.Sum(x => x.PrieceWithTax),

                    CurrencyId = SelectedCurrency.Id

                }, DocumentItems.ToList());
            }


            MessengerInstance.Send(UiActionMessage.RefreshDocumentsList);

            MessengerInstance.Send(UiActionMessage.BackToDocumentsList);
        }
        #endregion

        #region RemoveSelectedCommand

        private RelayCommand removeSelectedCommand;


        public ICommand RemoveSelectedCommand
        {
            get
            {
                if (removeSelectedCommand == null)
                {
                    removeSelectedCommand = new RelayCommand(OnRemoveSelected, CanExecureRemoveItem);
                }
                return removeSelectedCommand;
            }
        }
        private bool CanExecureRemoveItem(object obj)
        {
            return SelectedDocumentItem != null;
        }

        private void OnRemoveSelected(object obj)
        {
            documentsService.Remove(SelectedDocumentItem);
            DocumentItems.Remove(SelectedDocumentItem);
            if (documentToEdit != null)
            {
                documentToEdit.TotalPrieceNet = DocumentItems.Sum(x => x.PrieceNet);
                documentToEdit.TotalPrieceWithTax = DocumentItems.Sum(x => x.PrieceWithTax);
                documentsService.UpdateDocument(documentToEdit, new List<DocumentItem>());
            }

        }
        #endregion

        #region AddProductCommand


        private RelayCommand manageProductCommand;


        public ICommand ManageProductCommand
        {
            get
            {
                if (manageProductCommand == null)
                {
                    manageProductCommand = new RelayCommand(OnManageProduct);
                }
                return manageProductCommand;
            }
        }
        private void OnManageProduct(object obj)
        {
            var view = new ManageProductView();
            popUpWindow = new Window
            {
                Content = view
            };
            popUpWindow?.ShowDialog();
        }

        #endregion

        #region NumberEditable

        private bool numberEditable;
        public bool NumberEditable
        {
            get
            {
                return numberEditable;
            }
            set
            {
                numberEditable = value;

                RaisePropertyChanged(() => NumberEditable);
            }
        }

        #endregion

        #region DocumentItem

        private DocumentItem selectedDocumentItem;
        public DocumentItem SelectedDocumentItem
        {
            get
            {
                return selectedDocumentItem;
            }
            set
            {
                selectedDocumentItem = value;
                OnDocumentDataChanged();
                RaisePropertyChanged(() => SelectedDocumentItem);
            }
        }

        #endregion

        public string DocumentNumber
        {
            get
            {
                if (documentNumber == null)
                {
                    documentNumber = string.Empty;
                }
                return documentNumber;
            }
            set
            {
                documentNumber = value;
                RaisePropertyChanged(() => DocumentNumber);
            }
        }

        private void OnDocumentDataChanged()
        {
            if (SelectedClient == null || SelectedDocumentType == null)
            {
                return;
            }
            var num = documentsService.GenerateDocumentNumber(SelectedDocumentType, SelectedClient);
            if (num.State == Services.Enums.ServiceCallState.Error)
            {
                MessengerInstance.Send(new ShowDialogMessage(num.Message));
                return;
            }
            DocumentNumber = num.Result;
        }

        void OnReciveMessage(NewProductToDocumentMessage message)
        {
            if (message.docuemntItem == null)
            {
                return;
            }
            DocumentItems.Add(message.docuemntItem);
            popUpWindow?.Close();
            popUpWindow = null;
        }
        #region Taxes
        ObservableCollection<Tax> taxes;
        public ObservableCollection<Tax> Taxes
        {
            get
            {
                if (taxes == null)
                {
                    taxes = new ObservableCollection<Tax>();
                }
                return taxes;
            }
            set
            {
                taxes = value;
                RaisePropertyChanged(() => Taxes);
            }
        }
        #endregion

        #region Currency

        ObservableCollection<Currency> currencies;
        private Currency selectedCurrency;
        public ObservableCollection<Currency> Currencies
        {
            get
            {
                if (currencies == null)
                {
                    currencies = new ObservableCollection<Currency>();
                }
                return currencies;
            }
            set
            {
                currencies = value;
                RaisePropertyChanged(() => Currencies);
            }
        }
        [ExtRequired(ErrorMessage = "Wybierz walutę!")]
        public Currency SelectedCurrency
        {
            get
            {
                return selectedCurrency;
            }
            set
            {
                selectedCurrency = value;
                RaisePropertyChanged(() => SelectedCurrency);
            }
        }
        #endregion

        #region DocumentTypes
        ObservableCollection<DocumentItem> documentItems;
        ObservableCollection<DocumentType> documentTypes;
        private DocumentType selectedDocumentType;
        public ObservableCollection<DocumentItem> DocumentItems
        {
            get
            {
                if (documentItems == null)
                {
                    documentItems = new ObservableCollection<DocumentItem>();
                }
                return documentItems;
            }
            set
            {
                documentItems = value;

                RaisePropertyChanged(() => DocumentItems);
            }
        }

        public ObservableCollection<DocumentType> DocumentTypes
        {
            get
            {
                if (documentTypes == null)
                {
                    documentTypes = new ObservableCollection<DocumentType>();
                }
                return documentTypes;
            }
            set
            {
                documentTypes = value;

                RaisePropertyChanged(() => DocumentTypes);
            }
        }
        [ExtRequired(ErrorMessage = "Wybierz typ dokumentu!")]
        public DocumentType SelectedDocumentType
        {
            get
            {
                return selectedDocumentType;
            }
            set
            {
                selectedDocumentType = value;
                OnDocumentDataChanged();
                RaisePropertyChanged(() => SelectedDocumentType);
            }
        }
        #endregion
        #region Clients
        ObservableCollection<Client> clients;
        private Client selectedClient;
        public ObservableCollection<Client> Clients
        {
            get
            {
                if (clients == null)
                {
                    clients = new ObservableCollection<Client>();
                }
                return clients;
            }
            set
            {
                clients = value;
                RaisePropertyChanged(() => Clients);
            }
        }
        [ExtRequired(ErrorMessage ="Wybierz klienta!")]
        public Client SelectedClient
        {
            get
            {
                return selectedClient;
            }
            set
            {
                selectedClient = value;
                OnDocumentDataChanged();
                RaisePropertyChanged(() => SelectedClient);
            }
        }
        #endregion

    }
}
