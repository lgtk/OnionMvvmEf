﻿using Core.Entities;
using FirstFloor.ModernUI.Presentation;
using Services;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfClient.Attributes;
using WpfClient.Messages;

namespace WpfClient.ViewModel
{
    public class ManageProductViewModel : ViewModel
    {
        private readonly ITaxService taxService;
        private readonly IItemService itemsService;

        public ManageProductViewModel(IItemService itemsService, ITaxService taxService)
        {
            this.taxService = taxService;
            this.itemsService = itemsService;
            LoadData().ConfigureAwait(false);
            LoadTaxes().ConfigureAwait(false);
        }

        private async Task LoadTaxes()
        {
            var items = await taxService.GetTaxesAsync();
            if (items.State == Services.Enums.ServiceCallState.Error)
            {
                MessengerInstance.Send(new ShowDialogMessage(items.Message));
                return;
            }
            foreach (var item in items.Result)
            {
                Taxes.Add(item);
            }

        }

        private async Task LoadData()
        {
            Items?.Clear();
            var items = await itemsService.GetItemsAsync();
            if (items.State == Services.Enums.ServiceCallState.Error)
            {
                MessengerInstance.Send(new ShowDialogMessage(items.Message));
                return;
            }
            foreach (var item in items.Result)
            {
                Items.Add(item);
            }
        }

        private RelayCommand searchCommand;

        public ICommand SearchCommand
        {
            get
            {
                if (searchCommand == null)
                {
                    searchCommand = new RelayCommand(OnSearch, o => !string.IsNullOrWhiteSpace(Phrase));
                }
                return searchCommand;
            }
        }

        private RelayCommand saveCommand;

        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new RelayCommand(OnSaveCommand, CanExecuteSaveCommand);
                }
                return saveCommand;
            }
        }
        private bool CanExecuteSaveCommand(object o)
        {
            return SelectedItem?.Id > 0 && Count > 0 && NettoValue > 0;
        }
        private void OnSaveCommand(object o)
        {
            var product = new DocumentItem
            {
                Count = this.Count,
                ItemId = SelectedItem.Id,
                Item = SelectedItem,
                PrieceNet = NettoValue,
                PrieceWithTax = BruttoValue,



            };
            MessengerInstance.Send(new NewProductToDocumentMessage(product));

        }

        private void OnSearch(object obj)
        {
            var items = Phrase == string.Empty ? itemsService.GetItems() : itemsService.SearchItems(Phrase);
            Items.Clear();
            if (items.State == Services.Enums.ServiceCallState.Error)
            {
                MessengerInstance.Send(new ShowDialogMessage(items.Message));
                return;
            }
            foreach (var item in items.Result)
            {
                Items.Add(item);
            }
        }

        private string phrase;
        [ExtRequired(ErrorMessage = "Aby wyszukać element wpisz szukaną frazę!")]
        public override string Phrase
        {
            get
            {
                return phrase;
            }
            set
            {
                phrase = value;
                RaisePropertyChanged(() => Phrase);
                if (string.IsNullOrWhiteSpace(Phrase))
                {
                    LoadData().ConfigureAwait(false);
                }
            }
        }

        private string name;
        [ExtRequired(AllowEmptyStrings = false, ErrorMessage = "Wpisz nazwę produktu!")]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                RaisePropertyChanged(() => Name);

            }
        }

        private Item selectedItem;
        public Item SelectedItem
        {
            get
            {
                return selectedItem ?? (selectedItem = new Item());
            }
            set
            {
                selectedItem = value;
                Name = SelectedItem?.Name;
                RaisePropertyChanged(() => SelectedItem);
                if (value == null)
                {
                    return;
                }
                var tax = Taxes.FirstOrDefault(x => x.Id == selectedItem.TaxId);
                SelectedTax = tax;

                NettoValue = value.PrieceNet;

            }
        }

        private Tax selectedTax;
        [ExtRequired(AllowEmptyStrings = false, ErrorMessage = "Musisz wybrać podatek!")]
        public Tax SelectedTax
        {
            get
            {
                return selectedTax;
            }
            set
            {
                selectedTax = value;

                RaisePropertyChanged(() => SelectedTax);
                PrieceAvaliable = value != null;


            }
        }

        public ObservableCollection<Tax> Taxes { get; set; } = new ObservableCollection<Tax>();

        private decimal nettoValue;
        [ExtRequired(IsNumericStr =true, ErrorMessage ="Wprowadź wartość większą od 0!")]
        public decimal NettoValue
        {
            get
            {
                return nettoValue;
            }
            set
            {
                nettoValue = value;
                RaisePropertyChanged(() => NettoValue);

                NettoValueChanged();
            }
        }

        void NettoValueChanged()
        {
            if (SelectedItem == null)
            {
                return;
            }
            decimal tax = decimal.Divide(SelectedTax.Value, 100);
            var brutto = nettoValue * tax + nettoValue;
            bruttoValue = brutto;
            RaisePropertyChanged(() => BruttoValue);
        }

        void BruttoValueChanged()
        {
            if (SelectedItem == null)
            {
                return;
            }
            decimal tax = decimal.Divide(SelectedTax.Value, 100);
            var netto = bruttoValue - (bruttoValue * tax);

            nettoValue = netto;
            RaisePropertyChanged(() => NettoValue);

        }

        private decimal bruttoValue;
        [ExtRequired(IsNumericStr = true, ErrorMessage = "Wprowadź wartość większą od 0!")]
        public decimal BruttoValue
        {
            get
            {
                return bruttoValue;
            }
            set
            {
                bruttoValue = value;
                RaisePropertyChanged(() => BruttoValue);
                BruttoValueChanged();
            }
        }


        private bool prieceAvaliable;
        public bool PrieceAvaliable
        {
            get
            {
                return prieceAvaliable;
            }
            set
            {
                prieceAvaliable = value;
                RaisePropertyChanged(() => PrieceAvaliable);

            }
        }

        private int count;
        [ExtRequired(IsNumericStr = true, ErrorMessage = "Wprowadź wartość większą od 0!")]
        public int Count
        {
            get
            {
                return count;
            }
            set
            {
                count = value;
                RaisePropertyChanged(() => Count);

            }
        }

        ObservableCollection<Item> items;
        public ObservableCollection<Item> Items
        {
            get
            {
                if (items == null)
                {
                    items = new ObservableCollection<Item>();
                }
                return items;
            }
            set
            {
                items = value;
                RaisePropertyChanged(() => Items);
            }
        }


        private RelayCommand editCommand;

        /// <summary>
        /// Gets the MyCommand.
        /// </summary>
        public RelayCommand EditCommand
        {
            get
            {
                return editCommand
                    ?? (editCommand = new RelayCommand(
                    OnEditCommand, CanExecuteEditCommand));
            }
        }

        private void OnEditCommand(object obj)
        {
            itemsService.Edit(new Item
            {
                Id = SelectedItem.Id,
                SaveDate = SelectedItem.SaveDate,
                PrieceNet = NettoValue,
                PrieceWithTax = BruttoValue,
                TaxId = SelectedTax.Id,
                Name = Name
            });
            LoadData().ConfigureAwait(false);
        }

        private bool CanExecuteEditCommand(object o)
        {
            return SelectedItem?.Id > 0;
        }


        private RelayCommand addNewCommand;

        /// <summary>
        /// Gets the MyCommand.
        /// </summary>
        public RelayCommand AddNewCommand
        {
            get
            {
                return addNewCommand
                    ?? (addNewCommand = new RelayCommand(
                    OnAddCommand, CanExecuteAddCommand));
            }
        }

        private void OnAddCommand(object obj)
        {

            itemsService.AddNew(new Item
            {

                SaveDate = SelectedItem.SaveDate,
                PrieceNet = this.NettoValue,
                PrieceWithTax = this.BruttoValue,
                TaxId = SelectedTax.Id,
                Name = this.Name
            });
            LoadData().ConfigureAwait(false);
        }
        private bool CanExecuteAddCommand(object obj)
        {
            return SelectedItem != null && Count > 0 && NettoValue > 0;
        }


        private RelayCommand removeCommand;

        /// <summary>
        /// Gets the MyCommand.
        /// </summary>
        public RelayCommand RemoveCommand
        {
            get
            {
                return removeCommand
                    ?? (removeCommand = new RelayCommand(
                    OnRemoveCommand, CanExecuteRemoveCommand));
            }
        }

        private void OnRemoveCommand(object obj)
        {
            itemsService.Remove(SelectedItem);
            LoadData().ConfigureAwait(false);


        }
        private bool CanExecuteRemoveCommand(object obj)
        {
            return SelectedItem != null;
        }
    }

}
