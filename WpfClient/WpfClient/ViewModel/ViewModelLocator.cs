using Autofac;
using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using System.Windows.Navigation;
using WpfClient.View;

namespace WpfClient.ViewModel
{

    public class ViewModelLocator
    {
        public const string HomePage = "HomePage";
        public const string SecondPage = "SecondPage";
        private App application = App.Current as App;
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
           

        }
        public DocumentsViewModel Documents
        {
            get
            {
                return application.Container.Resolve<DocumentsViewModel>();
            }
        }

        public ManageDocumentViewModel ManageDocument
        {
            get
            {
                return application.Container.Resolve<ManageDocumentViewModel>();
            }
        }

        public ManageProductViewModel ManageProduct
        {
            get
            {
                return application.Container.Resolve<ManageProductViewModel>();
            }
        }
        public MainViewModel Main
        {
            get
            {
                return application.Container.Resolve<MainViewModel>();
            }
        }

        public static void Cleanup()
        {

        }
    }
}