﻿using Core.Entities;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Services.Interfaces;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfClient.Attributes;
using WpfClient.Messages;
using WpfClient.Session;

namespace WpfClient.ViewModel
{
    public class DocumentsViewModel : ViewModel
    {

        private readonly IDocumentsService documentService;
        private readonly SharedSession session;
        private RelayCommand addNewDocument;

        private ObservableCollection<Document> documents;

        private RelayCommand editSelectedCommand;

        private string phrase;

        private RelayCommand relayCommand;

        private RelayCommand removeSelected;

        private Document selectedDocument;

        public DocumentsViewModel(IDocumentsService documentService, SharedSession session)
        {
            this.documentService = documentService;
            this.session = session;
            Title = "Documents List";
            LoadDocuments().ConfigureAwait(false);
            MessengerInstance.Register<UiActionMessage>(this, OnUiMessageReceived);
        }

        public ICommand AddNewDocument
        {
            get
            {
                if (addNewDocument == null)
                {
                    addNewDocument = new RelayCommand(OnAddNewDocument);
                }
                return addNewDocument;
            }
        }

        public ObservableCollection<Document> Documents
        {
            get
            {
                if (documents == null)
                {
                    documents = new ObservableCollection<Document>();
                }
                return documents;
            }
            set
            {
                documents = value;
                RaisePropertyChanged(() => Documents);
            }
        }

        public RelayCommand EditSelectedCommand
        {
            get
            {
                return editSelectedCommand
                    ?? (editSelectedCommand = new RelayCommand(
                  OnEditSelected,
                    () => SelectedDocument != null));
            }
        }
        [ExtRequired(ErrorMessage = "Aby wyszukać element wpisz szukaną frazę!")]
        public override string Phrase
        {
            get
            {
                if (phrase == null)
                {
                    phrase = string.Empty;
                }
                return phrase;
            }
            set
            {
                phrase = value;
                if (string.IsNullOrWhiteSpace(phrase))
                {
                    LoadDocuments().ConfigureAwait(false);
                }
                RaisePropertyChanged(() => Phrase);
            }
        }

        public RelayCommand RemoveSelected
        {
            get
            {
                return removeSelected
                    ?? (removeSelected = new RelayCommand(
                  OnRemoveSelected,
                    () => SelectedDocument != null));
            }
        }

        /// <summary>
        /// Gets the SearchCommand.
        /// </summary>
        public RelayCommand SearchCommand
        {
            get
            {
                return relayCommand
                    ?? (relayCommand =
                    new RelayCommand(() => Search(), () => !string.IsNullOrWhiteSpace(Phrase)));
            }
        }

        public Document SelectedDocument
        {
            get
            {
                return selectedDocument;
            }
            set
            {
                selectedDocument = value;

                RaisePropertyChanged(() => SelectedDocument);
            }
        }

        private async Task LoadDocuments()
        {
            Documents.Clear();
            var docs = await documentService.GetDocumentsAsync();
            if (docs.State == Services.Enums.ServiceCallState.Error)
            {
                MessengerInstance.Send(new ShowDialogMessage(docs.Message));
                return;
            }
            foreach (var document in docs.Result)
            {
                Documents.Add(document);
            }
        }

        private void OnAddNewDocument()
        {
            session.SelectedDocument = null;
            Messenger.Default.Send(UiActionMessage.GoToManageDocument);
        }

        private void OnEditSelected()
        {
            session.SelectedDocument = SelectedDocument;
            Messenger.Default.Send(UiActionMessage.GoToManageDocument);


        }

        private async void OnRemoveSelected()
        {
            await documentService.RemoveAsync(SelectedDocument);
            await LoadDocuments();
        }

        private void OnUiMessageReceived(UiActionMessage message)
        {
            if (message == UiActionMessage.RefreshDocumentsList)
            {
                LoadDocuments().ConfigureAwait(false);
            }
        }
        private async void Search()
        {
            var docs = await documentService.SearchDocumentsAsync(Phrase);
            if(docs.State == Services.Enums.ServiceCallState.Error)
            {
                MessengerInstance.Send(new ShowDialogMessage(docs.Message));
                return;
            }
            Documents.Clear();

            foreach (var doc in docs.Result)
            {
                Documents.Add(doc);
            }
        }
    }
}