﻿using GalaSoft.MvvmLight;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using WpfClient.Attributes;

namespace WpfClient.ViewModel
{
    public abstract class ViewModel : ViewModelBase, IDataErrorInfo
    {
        private void DisplayDialog(string message)
        {

        }

        private string title;
        public string Title
        {
            get
            {
                if (title == null)
                {
                    title = string.Empty;
                }
                return title;
            }
            set
            {
                title = value;

                RaisePropertyChanged(() => Title);
            }
        }
        [ExtRequired(ErrorMessage ="Aby wyszukać element wpisz szukaną frazę!")]
        public virtual string Phrase { get; set; }

        public string ValidationByReflection(string propName)
        {
            var propWithReq = GetType().GetProperties().FirstOrDefault(x => x.Name.ToUpper() == propName.ToUpper());
            if (propWithReq == null)
            {
                return null;
            }

            var attr = propWithReq.GetCustomAttributes(true).FirstOrDefault(x => x is ExtRequiredAttribute);
            if (attr == null)
            {
                return null;
            }
            var extAttr = attr as ExtRequiredAttribute;
            var requiredMsg = extAttr?.ErrorMessage;
            var propVal = propWithReq.GetValue(this);
            if (propVal == null)
            {
                return requiredMsg;
            }
            var isString = propVal is string;
            var isNumeric = propVal is decimal || propVal is int;
            decimal numericVal = decimal.Zero;
            var strVal = propVal as string;
            if (isString  && string.IsNullOrWhiteSpace(strVal))
            {
                return requiredMsg;
            }
            if(((extAttr?.IsNumericStr).GetValueOrDefault() || isNumeric || isString) &&  decimal.TryParse(propVal.ToString(), out numericVal) && numericVal <= 0)
            {
                return requiredMsg;
            }
            return null;
        }

        public string Error => string.Empty;

        public string this[string columnName]
        {

            get
            {
                return ValidationByReflection(columnName);
            }
        }
    }
}
