﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient.Session
{
    public class SharedSession
    {
        public Document SelectedDocument { get; set; }
    }
}
