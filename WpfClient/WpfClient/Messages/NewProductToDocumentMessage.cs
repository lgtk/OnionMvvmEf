﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient.Messages
{
    public class NewProductToDocumentMessage
    {
        public NewProductToDocumentMessage(DocumentItem item)
        {
            this.docuemntItem = item;
        }

        public DocumentItem docuemntItem { get; private set; }
    }
}
