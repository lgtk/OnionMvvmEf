﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient.Messages
{
    public class UpdateWindowTitle
    {
        public string NewTitle { get; private set; }
        public UpdateWindowTitle(string newTitle)
        {
            NewTitle = newTitle;
        }
    }
}
