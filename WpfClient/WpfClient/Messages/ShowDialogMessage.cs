﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient.Messages
{
    public class ShowDialogMessage
    {
        public string Message { get; private set; }
        public ShowDialogMessage(string message)
        {
            Message = message;
        }
    }
}
