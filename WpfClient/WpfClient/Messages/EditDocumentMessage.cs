﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient.Messages
{
    public class EditDocumentMessage
    {
        public EditDocumentMessage(Document item)
        {
            this.document = item;
        }

        public Document document { get; private set; }
    }
}
