﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient.Messages
{
    public enum UiActionMessage : ushort
    {
        Default,
        RefreshDocumentsList,
        GoToManageDocument,
        ClosePopUpWindow,
        BackToDocumentsList
    }
}
