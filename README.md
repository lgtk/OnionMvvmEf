# OnionMvvmEf
Stack:
1. Wpf
2. MvvmLight
3. Nlog
4. FirstFloor Modern Ui
5. EntityFramework code first
6. RestSharp

Im trying implement onion architecture for with WPF app. Between ViewModel and Service we should not share DbModel in real life. Its very small project so i'm dont do it.

GeneratingDB:

1. WpfClient -> App.Config -> set connection string to your data base

2. In Package Manager Console
add-migration MsSql -StartUpProjectName Infrastructure -Project Infrastructure
update-database -StartUpProjectName Infrastructure -Project Infrastructure  -ConnectionString "Data Source=...;Initial Catalog=...;User Id=...;Password=..."   -ConnectionProviderName System.Data.SqlClient

3. Run

IMPORTANT In debug mode app generate some datas like dictionaries, clients and products. Im not implement CRUD' operations only for Documents, and Items.
